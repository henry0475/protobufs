// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.2
// source: orderservice.proto

package orderservicepb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// OrderServiceClient is the client API for OrderService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type OrderServiceClient interface {
	PlaceOrder(ctx context.Context, in *PlaceOrderRequest, opts ...grpc.CallOption) (*PlaceOrderReply, error)
	// DeliveredOrder 当订单配送完成后，写入消息队列的事件中
	DeliveredOrder(ctx context.Context, in *DeliveredOrderRequest, opts ...grpc.CallOption) (*DeliveredOrderReply, error)
	// GetOrderUID 获取订单的归属用户UID
	GetOrderUID(ctx context.Context, in *GetOrderUIDRequest, opts ...grpc.CallOption) (*GetOrderUIDReply, error)
	// GetHistoryOrders 获取用户的历史订单 订单排序使用last_modification_time字段倒序
	GetHistoryOrders(ctx context.Context, in *GetHistoryOrdersRequest, opts ...grpc.CallOption) (*GetHistoryOrdersReplay, error)
	// GetOrderInfo 通过订单id获取该订单信息
	GetOrderInfo(ctx context.Context, in *GetOrderInfoRequest, opts ...grpc.CallOption) (*GetOrderInfoReply, error)
	// GetShouldPayAmount 获取某个订单的待支付金额
	// 如果返回0，表示已经支付完成，没有需要待支付的
	// 可以用于微信支付所使用的amount
	GetShouldPayAmount(ctx context.Context, in *GetShouldPayAmountRequest, opts ...grpc.CallOption) (*GetShouldPayAmountReply, error)
	// PayOrder 使用银行卡支付某个订单
	// 如果是微信支付，会在gateway那里获取到参数，直接返回
	PayOrder(ctx context.Context, in *PayOrderRequest, opts ...grpc.CallOption) (*PayOrderReply, error)
	// GetVIPSavedMoney 获取某个订单的会员节省金额，$3.34会被标示为334
	GetVIPSavedMoney(ctx context.Context, in *GetVIPSavedMoneyRequest, opts ...grpc.CallOption) (*GetVIPSavedMoneyReply, error)
	// GetOrderedItems 获取某个用户订单下的所有餐品信息
	GetOrderedItems(ctx context.Context, in *GetOrderedItemsRequest, opts ...grpc.CallOption) (*GetOrderedItemsReply, error)
	// GetOrderedItemsForRestaurant 获取餐厅的订单列表
	// 可以获取某个班次下的某个餐厅的的菜品信息
	GetOrderedItemsForRestaurant(ctx context.Context, in *GetOrderedItemsForRestaurantRequest, opts ...grpc.CallOption) (*GetOrderedItemsForRestaurantReply, error)
}

type orderServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewOrderServiceClient(cc grpc.ClientConnInterface) OrderServiceClient {
	return &orderServiceClient{cc}
}

func (c *orderServiceClient) PlaceOrder(ctx context.Context, in *PlaceOrderRequest, opts ...grpc.CallOption) (*PlaceOrderReply, error) {
	out := new(PlaceOrderReply)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/PlaceOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) DeliveredOrder(ctx context.Context, in *DeliveredOrderRequest, opts ...grpc.CallOption) (*DeliveredOrderReply, error) {
	out := new(DeliveredOrderReply)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/DeliveredOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) GetOrderUID(ctx context.Context, in *GetOrderUIDRequest, opts ...grpc.CallOption) (*GetOrderUIDReply, error) {
	out := new(GetOrderUIDReply)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/GetOrderUID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) GetHistoryOrders(ctx context.Context, in *GetHistoryOrdersRequest, opts ...grpc.CallOption) (*GetHistoryOrdersReplay, error) {
	out := new(GetHistoryOrdersReplay)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/GetHistoryOrders", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) GetOrderInfo(ctx context.Context, in *GetOrderInfoRequest, opts ...grpc.CallOption) (*GetOrderInfoReply, error) {
	out := new(GetOrderInfoReply)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/GetOrderInfo", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) GetShouldPayAmount(ctx context.Context, in *GetShouldPayAmountRequest, opts ...grpc.CallOption) (*GetShouldPayAmountReply, error) {
	out := new(GetShouldPayAmountReply)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/GetShouldPayAmount", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) PayOrder(ctx context.Context, in *PayOrderRequest, opts ...grpc.CallOption) (*PayOrderReply, error) {
	out := new(PayOrderReply)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/PayOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) GetVIPSavedMoney(ctx context.Context, in *GetVIPSavedMoneyRequest, opts ...grpc.CallOption) (*GetVIPSavedMoneyReply, error) {
	out := new(GetVIPSavedMoneyReply)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/GetVIPSavedMoney", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) GetOrderedItems(ctx context.Context, in *GetOrderedItemsRequest, opts ...grpc.CallOption) (*GetOrderedItemsReply, error) {
	out := new(GetOrderedItemsReply)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/GetOrderedItems", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) GetOrderedItemsForRestaurant(ctx context.Context, in *GetOrderedItemsForRestaurantRequest, opts ...grpc.CallOption) (*GetOrderedItemsForRestaurantReply, error) {
	out := new(GetOrderedItemsForRestaurantReply)
	err := c.cc.Invoke(ctx, "/orderservicepb.OrderService/GetOrderedItemsForRestaurant", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// OrderServiceServer is the server API for OrderService service.
// All implementations must embed UnimplementedOrderServiceServer
// for forward compatibility
type OrderServiceServer interface {
	PlaceOrder(context.Context, *PlaceOrderRequest) (*PlaceOrderReply, error)
	// DeliveredOrder 当订单配送完成后，写入消息队列的事件中
	DeliveredOrder(context.Context, *DeliveredOrderRequest) (*DeliveredOrderReply, error)
	// GetOrderUID 获取订单的归属用户UID
	GetOrderUID(context.Context, *GetOrderUIDRequest) (*GetOrderUIDReply, error)
	// GetHistoryOrders 获取用户的历史订单 订单排序使用last_modification_time字段倒序
	GetHistoryOrders(context.Context, *GetHistoryOrdersRequest) (*GetHistoryOrdersReplay, error)
	// GetOrderInfo 通过订单id获取该订单信息
	GetOrderInfo(context.Context, *GetOrderInfoRequest) (*GetOrderInfoReply, error)
	// GetShouldPayAmount 获取某个订单的待支付金额
	// 如果返回0，表示已经支付完成，没有需要待支付的
	// 可以用于微信支付所使用的amount
	GetShouldPayAmount(context.Context, *GetShouldPayAmountRequest) (*GetShouldPayAmountReply, error)
	// PayOrder 使用银行卡支付某个订单
	// 如果是微信支付，会在gateway那里获取到参数，直接返回
	PayOrder(context.Context, *PayOrderRequest) (*PayOrderReply, error)
	// GetVIPSavedMoney 获取某个订单的会员节省金额，$3.34会被标示为334
	GetVIPSavedMoney(context.Context, *GetVIPSavedMoneyRequest) (*GetVIPSavedMoneyReply, error)
	// GetOrderedItems 获取某个用户订单下的所有餐品信息
	GetOrderedItems(context.Context, *GetOrderedItemsRequest) (*GetOrderedItemsReply, error)
	// GetOrderedItemsForRestaurant 获取餐厅的订单列表
	// 可以获取某个班次下的某个餐厅的的菜品信息
	GetOrderedItemsForRestaurant(context.Context, *GetOrderedItemsForRestaurantRequest) (*GetOrderedItemsForRestaurantReply, error)
	mustEmbedUnimplementedOrderServiceServer()
}

// UnimplementedOrderServiceServer must be embedded to have forward compatible implementations.
type UnimplementedOrderServiceServer struct {
}

func (UnimplementedOrderServiceServer) PlaceOrder(context.Context, *PlaceOrderRequest) (*PlaceOrderReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method PlaceOrder not implemented")
}
func (UnimplementedOrderServiceServer) DeliveredOrder(context.Context, *DeliveredOrderRequest) (*DeliveredOrderReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeliveredOrder not implemented")
}
func (UnimplementedOrderServiceServer) GetOrderUID(context.Context, *GetOrderUIDRequest) (*GetOrderUIDReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOrderUID not implemented")
}
func (UnimplementedOrderServiceServer) GetHistoryOrders(context.Context, *GetHistoryOrdersRequest) (*GetHistoryOrdersReplay, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetHistoryOrders not implemented")
}
func (UnimplementedOrderServiceServer) GetOrderInfo(context.Context, *GetOrderInfoRequest) (*GetOrderInfoReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOrderInfo not implemented")
}
func (UnimplementedOrderServiceServer) GetShouldPayAmount(context.Context, *GetShouldPayAmountRequest) (*GetShouldPayAmountReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetShouldPayAmount not implemented")
}
func (UnimplementedOrderServiceServer) PayOrder(context.Context, *PayOrderRequest) (*PayOrderReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method PayOrder not implemented")
}
func (UnimplementedOrderServiceServer) GetVIPSavedMoney(context.Context, *GetVIPSavedMoneyRequest) (*GetVIPSavedMoneyReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetVIPSavedMoney not implemented")
}
func (UnimplementedOrderServiceServer) GetOrderedItems(context.Context, *GetOrderedItemsRequest) (*GetOrderedItemsReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOrderedItems not implemented")
}
func (UnimplementedOrderServiceServer) GetOrderedItemsForRestaurant(context.Context, *GetOrderedItemsForRestaurantRequest) (*GetOrderedItemsForRestaurantReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOrderedItemsForRestaurant not implemented")
}
func (UnimplementedOrderServiceServer) mustEmbedUnimplementedOrderServiceServer() {}

// UnsafeOrderServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to OrderServiceServer will
// result in compilation errors.
type UnsafeOrderServiceServer interface {
	mustEmbedUnimplementedOrderServiceServer()
}

func RegisterOrderServiceServer(s grpc.ServiceRegistrar, srv OrderServiceServer) {
	s.RegisterService(&OrderService_ServiceDesc, srv)
}

func _OrderService_PlaceOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PlaceOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).PlaceOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/PlaceOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).PlaceOrder(ctx, req.(*PlaceOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_DeliveredOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeliveredOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).DeliveredOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/DeliveredOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).DeliveredOrder(ctx, req.(*DeliveredOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_GetOrderUID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetOrderUIDRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).GetOrderUID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/GetOrderUID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).GetOrderUID(ctx, req.(*GetOrderUIDRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_GetHistoryOrders_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetHistoryOrdersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).GetHistoryOrders(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/GetHistoryOrders",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).GetHistoryOrders(ctx, req.(*GetHistoryOrdersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_GetOrderInfo_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetOrderInfoRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).GetOrderInfo(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/GetOrderInfo",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).GetOrderInfo(ctx, req.(*GetOrderInfoRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_GetShouldPayAmount_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetShouldPayAmountRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).GetShouldPayAmount(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/GetShouldPayAmount",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).GetShouldPayAmount(ctx, req.(*GetShouldPayAmountRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_PayOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PayOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).PayOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/PayOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).PayOrder(ctx, req.(*PayOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_GetVIPSavedMoney_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetVIPSavedMoneyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).GetVIPSavedMoney(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/GetVIPSavedMoney",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).GetVIPSavedMoney(ctx, req.(*GetVIPSavedMoneyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_GetOrderedItems_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetOrderedItemsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).GetOrderedItems(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/GetOrderedItems",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).GetOrderedItems(ctx, req.(*GetOrderedItemsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_GetOrderedItemsForRestaurant_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetOrderedItemsForRestaurantRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).GetOrderedItemsForRestaurant(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orderservicepb.OrderService/GetOrderedItemsForRestaurant",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).GetOrderedItemsForRestaurant(ctx, req.(*GetOrderedItemsForRestaurantRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// OrderService_ServiceDesc is the grpc.ServiceDesc for OrderService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var OrderService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "orderservicepb.OrderService",
	HandlerType: (*OrderServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "PlaceOrder",
			Handler:    _OrderService_PlaceOrder_Handler,
		},
		{
			MethodName: "DeliveredOrder",
			Handler:    _OrderService_DeliveredOrder_Handler,
		},
		{
			MethodName: "GetOrderUID",
			Handler:    _OrderService_GetOrderUID_Handler,
		},
		{
			MethodName: "GetHistoryOrders",
			Handler:    _OrderService_GetHistoryOrders_Handler,
		},
		{
			MethodName: "GetOrderInfo",
			Handler:    _OrderService_GetOrderInfo_Handler,
		},
		{
			MethodName: "GetShouldPayAmount",
			Handler:    _OrderService_GetShouldPayAmount_Handler,
		},
		{
			MethodName: "PayOrder",
			Handler:    _OrderService_PayOrder_Handler,
		},
		{
			MethodName: "GetVIPSavedMoney",
			Handler:    _OrderService_GetVIPSavedMoney_Handler,
		},
		{
			MethodName: "GetOrderedItems",
			Handler:    _OrderService_GetOrderedItems_Handler,
		},
		{
			MethodName: "GetOrderedItemsForRestaurant",
			Handler:    _OrderService_GetOrderedItemsForRestaurant_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "orderservice.proto",
}
