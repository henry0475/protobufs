syntax = "proto3";

package merchantservicepb;

option go_package = "gitlab.com/henry0475/protobufs/yumontime/merchantservice/merchantservicepb";

// MerchantService 商家服务
// 分为两类，第一类是餐厅商家；第二类是线上折扣优惠商户
service MerchantService {
    // 对商家的操作 author houzhenkai
    // AddMerchant 添加商家
    rpc AddMerchant(AddMerchantRequest) returns(AddMerchantReply);
    // GetMerchants 获取商家列表
    rpc GetMerchants(GetMerchantsRequest) returns(GetMerchantsReply);
    // DeleteMerchant 删除商家
    rpc DeleteMerchant(DeleteMerchantRequest) returns(DeleteMerchantReply);
    // UpdateMerchant 修改商家
    rpc UpdateMerchant(UpdateMerchantRequest) returns(UpdateMerchantReply);
    // 获取商家名称
    rpc GetMerchantsNames(GetMerchantsNamesRequest) returns(GetMerchantsNamesReply);

    // GetRestaurantsNames 基于餐厅ID获取餐厅的名称
    // 此接口暂停使用，后续统一使用GetMerchantsNames接口，-- 北京时间2022-8-14 15:45:11
    rpc GetRestaurantsNames(GetRestaurantsNamesRequest) returns(GetRestaurantsNamesReply) ;
    // GetMenuItem 基于菜品ID获取菜单上的某项信息
    rpc GetMenuItem(GetMenuItemRequest) returns(GetMenuItemReply) ;
    
    
    // 商家分类，例如：「美食」、「景点」、「租房」  author  houzhenkai
    // AddTreasure 商家商品操作-新增商品
    rpc AddTreasure(AddTreasureRequest) returns(AddTreasureReply);
    // GetTreasures 获取商品列表
    rpc GetTreasures(GetTreasuresRequest) returns(GetTreasuresReply);
    // DeleteTreasure 停用/启用(删除恢复)一个商家类型
    rpc DeleteTreasure(DeleteTreasureRequest) returns(DeleteTreasureReply);
    // UpdateTreasure 修改商品
    rpc UpdateTreasure(UpdateTreasureRequest) returns(UpdateTreasureReply);

    // 库存商品 author houzhenkai
    // AddInventory 添加库存商品
    rpc AddInventory(AddInventoryRequest) returns(AddInventoryReply);
    // GetInventorys获取商品列表
    rpc GetInventorys(GetInventorysRequest) returns(GetInventorysReply);
    // DeleteInventory 停用/删除一个商家类型
    rpc DeleteInventory(DeleteInventoryRequest) returns(DeleteInventoryReply);
    // UpdateInventory 修改商品
    rpc UpdateInventory(UpdateInventoryRequest) returns(UpdateInventoryReply);
}
// Limit 用来做分页的一些参数
message Limit {
    int64 page = 1; // 分页的页数
    int64 size = 2; // 每页的大小
}
// Address 地址信息
message Address{
    string address = 1; // 详细地址描述
    int64 service_area_id = 2; //服务大区的ID
    float longtitude = 3;// 经度
    float latitude = 4; // 纬度
}
// Merchant 商家的基础信息
message Merchant {
    message Metadata {
        int64 likes = 1;
        int64 views = 2;
        int64 comments = 3;
    }
    int64 id = 1;
    string uid =2;
    string name = 3;
    string logo_url = 4;
    string description = 5;
    string mobile = 6;
    // 营业时间，格式是  08:00 - 22:00，只用来做展示
    string business_from = 7;// 营业开始时间，通常是每天的几点开始营业 12:00
    string business_to = 8;// 营业结束时间，通常是每天的几点关门 12:00
    Metadata metadata = 9;
    Address address = 10; //  地址区域信息
    // repeated Treasure treasures = 6;
}
// AddMerchantRequest 添加一个商家
message AddMerchantRequest {
    Merchant merchant = 1;
}
message AddMerchantReply{
    int64 id = 1;// 添加成功后的商家的主键ID
}
// GetMerchantsRequest 获取商家列表
message GetMerchantsRequest {
    Address address = 1;
    string order = 2; // 排序 如 'id desc'
    Limit limit = 3;// 分页参数
}
message GetMerchantsReply {
    repeated Merchant merchants = 1;
}
// DeleteMerchantRequest 删除一个商家
message DeleteMerchantRequest {
    int64 id = 1; 
}
message DeleteMerchantReply{
}
// UpdateMerchantRequest 更新商家信息
message UpdateMerchantRequest{
    Merchant merchant = 1;
}
message UpdateMerchantReply{    
}

message GetMenuItemRequest {
    int64 id = 1;
}
message GetMenuItemReply {
    MenuItem menu_item = 1;
}

message MenuItem {
    message Name {
        string english = 1;
        string chinese = 2;
    }
    message Description {
        string english = 1;
        string chinese = 2;
    }
    enum Status {
        Valid = 0;
        SoldOutToday = 1;   // 今日已售罄
        SoldOut = 2;        // 无限期下架
        Deleted = 3;        // 删除态
    }
    message Value {
        enum Category {
            Money = 0;      // 需要使用钱结算
            Point = 1;      // 可以使用积分结算
        }
        
        Category category = 1;  // 结算类别
        int64 base = 2;         // 基础价格，$5.4 -> 540
        int64 sale = 3;         // 销售价格
        int64 normal = 4;       // 正常价格
    }
    message DIY {
        int64 id = 1;
        int64 category_id = 2;
        Name name = 3;
        Value value = 4;
    }

    int64 id = 1;               // 商品ID
    int64 restaurant_id = 2;    // 餐厅ID
    Name name = 3;
    string image_path = 4;
    Description description = 5;
    Status status = 6;
    Value value = 7;
    bool allowed_diy = 8;       // 是否允许DIY
    repeated DIY diys = 9;      // 该商品所包含的DIY信息
}

message GetRestaurantsNamesRequest {
    repeated int64 ids = 1;
}
message GetRestaurantsNamesReply {
    message Merchant {
        int64 id = 1;
        string chinese_name = 2;
        string english_name = 3;
    }
    map<int64, Merchant> merchants = 2;
}


message GetMerchantsNamesRequest {
    repeated int64 ids = 1;
}
message GetMerchantsNamesReply {
    message Merchant {
        int64 id = 1;
        string chinese_name = 2;
        string english_name = 3;
    }
    map<int64, Merchant> merchants = 2;
}

// Treasure 商家的商品
message Treasure {
    message Metadata {
        int64 likes = 1; //  点赞量
        int64 views = 2;// 访问量
        int64 sales = 3; // 销量== 兑换量
        int64 storage_left = 4; // 剩余量=库存-销量
        int64 comments = 5;// 评论数
    }
    // Value 整型，如果是$的话，需要/100使用；如果是积分的话，直接使用
    message Value {
        int64 price = 1; // 实际需要支付的价格
        int64 normal_price = 2; // 对外原价，仅供展示
        int64 base_price = 3;// 对商家展示的价格
        int64 point = 4; // 实际需要支付的积分
        int64 normal_point = 5; // 对外积分，仅供展示
    }
    // 券码类型
    enum CodeType {
        Unknown = 0;
        Code = 1;  // 代码
        Image = 2; // 图片
    }
    int64 merchant_id = 1;
    int64 type_id = 2;
    string name = 3;
    string description = 4;
    DeliveryType delivery_type = 7; // 交付类型
    string callback_url = 8;
    int64 validity = 9;// 有效期，自兑换时间开始，多少小时
    int64 exchange_from = 10; // 兑换开始时间
    int64 exchange_to = 11; //兑换结束时间
    string images = 12; // 商品图片
    int64 inventory_nums = 13; // 库存剩余数量
    Value value = 14; // 价格/积分信息
    Metadata metadata = 15; // 点赞、浏览量等其他数据
    CodeType code_type = 16; // 1券码2图片
}

// merchant_type 表的字段
enum Status {
    Unknown = 0;// 未知
    Enable = 1;
    Deactivation = 2;
}
//  AddMerchantTypeRequest 添加一个商家分类
message AddMerchantTypeRequest {
    string creator = 1; // 创建人的UID
    string chinese_name = 2;// 中文名
    string english_name = 3;// 英文名
}
message AddMerchantTypeReply {
    int64 ID = 1;
}
//  MerchantType 商家分类
message MerchantType {
    int64 ID = 1; // 分类ID
    string chinese_name = 2; // 中文名
    string english_name = 3;// 英文名
    string creator = 4; // 创建人
    Status status = 5; // 状态，启用/停用
} 
// GetMerchantTypesRequest 获取商家分类列表
message GetMerchantTypesRequest {
    string creator = 1;// 根据创建人来筛选
    Status status = 2; // 启用或者停用
    string order = 3; // 排序 如 'id desc'
    Limit limit = 4;// 分页参数
}
message GetMerchantTypesReply {
    repeated MerchantType  merchant_types = 1; 
}
//  DeactivationMerchantTypeRequest 删除停用商家分类
message DeactivationMerchantTypeRequest {
    int64 ID = 1;// 
    Status status = 2;// 启用或者停用
}
message DeactivationMerchantTypeReply {}
//  UpdateMerchantTypeRequest 更新商家分类
message UpdateMerchantTypeRequest{
    int64 ID = 1;// 类型ID
    string chinese_name = 2;// 中文名
    string english_name = 3;// 英文名
    
}
message UpdateMerchantTypeReply {}

// DeliveryType 交付类型
enum DeliveryType{
    UnknownDelivery = 0; // 未知交付类型，通常不用
    PrepaidInventory = 1; // 预付库存
    Callback = 2; // 回调
}

// AddTreasureRequest 新增商品
message AddTreasureRequest {
    Treasure treasure = 1;
}
message AddTreasureReply {
}
// GetTreasuresRequest 获取商品列表
message GetTreasuresRequest {
    int64 merchant_id = 1;
    DeliveryType delivery_type = 2;
    string order = 3; // 排序 如 'id desc'
    Limit limit = 4;// 分页参数
}
message GetTreasuresReply {
    repeated Treasure treasure = 1;
}
// DeleteTreasureRequest 删除商品
message DeleteTreasureRequest{
    int64 id = 1;
}
message DeleteTreasureReply{
}
// UpdateTreasureRequest 更新商品
message UpdateTreasureRequest {
    Treasure treasure =1;
}
message UpdateTreasureReply {   
}

///// 商品库存相关 ////
// InventoryStatus 库存状态
enum InventoryStatus {
    // 1可兑换,2已删除,3已过期,4已使用,5已兑换
    _ = 0;
    Convertible = 1;
    Deleted = 2;
    Expired = 3;
    Used = 4;
    Redeemed = 5;
}
// Inventory 商品库存
message Inventory {
    int64 id = 1;
    int64 item_id = 2;
    string code = 3;
    int64 valid_from = 4;
    int64 valid_to = 5;
    InventoryStatus status = 6;
}
// AddInventoryRequest 添加一条库存商品
message AddInventoryRequest {
    Inventory inventory = 1;
}
message AddInventoryReply{
    int64 id = 1;
}
// GetInventorysRequest 获取库存列表
message GetInventorysRequest {
    int64 item_id = 1;
    InventoryStatus status = 2;
    string order = 3; // 排序 如 'id desc'
    Limit limit = 4;// 分页参数
}
message GetInventorysReply {
    repeated Inventory inventorys = 1;
}
// DeleteInventoryRequest 删除库存商品
message DeleteInventoryRequest{
    int64 id = 1;
}
message DeleteInventoryReply{
}
// UpdateInventoryRequest 更新库存里的商品
message UpdateInventoryRequest{
    Inventory inventory = 1;
}
message UpdateInventoryReply{
}