// Code generated by protoc-gen-hrpc. DO NOT EDIT.
// versions:
// - protoc-gen-hrpc v1.0.0
// - protoc          v3.21.2
// source: location.proto

package locationpb

import (
	context "context"
	client "git.zd.zone/hrpc/hrpc/client"
	service "git.zd.zone/hrpc/hrpc/service"
	grpc "google.golang.org/grpc"
	insecure "google.golang.org/grpc/credentials/insecure"
)

var srv *locationService

type locationService struct {
	conn   *grpc.ClientConn
	Client LocationServiceClient
}

func init() {
	client.Register(connectLocationService)
}

// GetGPS for getting GPS info based on the address you provided
func GetGPS(ctx context.Context, in *GetGPSRequest, opts ...grpc.CallOption) (*GetGPSReply, error) {
	return srv.Client.GetGPS(ctx, in, opts...)
}

func GetStandardAddress(ctx context.Context, in *GetStandardAddressRequest, opts ...grpc.CallOption) (*GetStandardAddressReply, error) {
	return srv.Client.GetStandardAddress(ctx, in, opts...)
}

func ParseOriginalAddress(ctx context.Context, in *ParseOriginalAddressRequest, opts ...grpc.CallOption) (*ParseOriginalAddressReply, error) {
	return srv.Client.ParseOriginalAddress(ctx, in, opts...)
}

func LocateIn(ctx context.Context, in *LocateInRequest, opts ...grpc.CallOption) (*LocateInReply, error) {
	return srv.Client.LocateIn(ctx, in, opts...)
}

func GetTSPRoute(ctx context.Context, in *GetTSPRouteRequest, opts ...grpc.CallOption) (*GetTSPRouteReply, error) {
	return srv.Client.GetTSPRoute(ctx, in, opts...)
}

func GetAreaInfo(ctx context.Context, in *GetAreaInfoRequest, opts ...grpc.CallOption) (*GetAreaInfoReply, error) {
	return srv.Client.GetAreaInfo(ctx, in, opts...)
}

func IsPickupAddress(ctx context.Context, in *IsPickupAddressRequest, opts ...grpc.CallOption) (*IsPickupAddressReply, error) {
	return srv.Client.IsPickupAddress(ctx, in, opts...)
}

func GetAllPickupAddresses(ctx context.Context, in *GetAllPickupAddressesRequest, opts ...grpc.CallOption) (*GetAllPickupAddressesReply, error) {
	return srv.Client.GetAllPickupAddresses(ctx, in, opts...)
}

func connectLocationService() error {
	s, err := service.Get("locationservice")
	if err != nil {
		return err
	}
	c, err := grpc.Dial(s.Target(), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return err
	}
	srv = &locationService{
		conn:   c,
		Client: NewLocationServiceClient(c),
	}
	return nil
}
