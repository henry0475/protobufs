// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.21.2
// source: limitationservice.proto

package limitationservicepb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type GetAvailableTimesRequest_Category int32

const (
	GetAvailableTimesRequest_MOMENTS        GetAvailableTimesRequest_Category = 0
	GetAvailableTimesRequest_DIRECT_MESSAGE GetAvailableTimesRequest_Category = 1
	GetAvailableTimesRequest_COUPONS        GetAvailableTimesRequest_Category = 2
)

// Enum value maps for GetAvailableTimesRequest_Category.
var (
	GetAvailableTimesRequest_Category_name = map[int32]string{
		0: "MOMENTS",
		1: "DIRECT_MESSAGE",
		2: "COUPONS",
	}
	GetAvailableTimesRequest_Category_value = map[string]int32{
		"MOMENTS":        0,
		"DIRECT_MESSAGE": 1,
		"COUPONS":        2,
	}
)

func (x GetAvailableTimesRequest_Category) Enum() *GetAvailableTimesRequest_Category {
	p := new(GetAvailableTimesRequest_Category)
	*p = x
	return p
}

func (x GetAvailableTimesRequest_Category) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (GetAvailableTimesRequest_Category) Descriptor() protoreflect.EnumDescriptor {
	return file_limitationservice_proto_enumTypes[0].Descriptor()
}

func (GetAvailableTimesRequest_Category) Type() protoreflect.EnumType {
	return &file_limitationservice_proto_enumTypes[0]
}

func (x GetAvailableTimesRequest_Category) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use GetAvailableTimesRequest_Category.Descriptor instead.
func (GetAvailableTimesRequest_Category) EnumDescriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{3, 0}
}

type StartDeductionRequest_Category int32

const (
	StartDeductionRequest_MOMENTS        StartDeductionRequest_Category = 0
	StartDeductionRequest_DIRECT_MESSAGE StartDeductionRequest_Category = 1
	StartDeductionRequest_COUPONS        StartDeductionRequest_Category = 2
)

// Enum value maps for StartDeductionRequest_Category.
var (
	StartDeductionRequest_Category_name = map[int32]string{
		0: "MOMENTS",
		1: "DIRECT_MESSAGE",
		2: "COUPONS",
	}
	StartDeductionRequest_Category_value = map[string]int32{
		"MOMENTS":        0,
		"DIRECT_MESSAGE": 1,
		"COUPONS":        2,
	}
)

func (x StartDeductionRequest_Category) Enum() *StartDeductionRequest_Category {
	p := new(StartDeductionRequest_Category)
	*p = x
	return p
}

func (x StartDeductionRequest_Category) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (StartDeductionRequest_Category) Descriptor() protoreflect.EnumDescriptor {
	return file_limitationservice_proto_enumTypes[1].Descriptor()
}

func (StartDeductionRequest_Category) Type() protoreflect.EnumType {
	return &file_limitationservice_proto_enumTypes[1]
}

func (x StartDeductionRequest_Category) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use StartDeductionRequest_Category.Descriptor instead.
func (StartDeductionRequest_Category) EnumDescriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{5, 0}
}

type GetAllAvailableTimesRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Uid    string `protobuf:"bytes,1,opt,name=uid,proto3" json:"uid,omitempty"`
	AreaId int64  `protobuf:"varint,2,opt,name=area_id,json=areaId,proto3" json:"area_id,omitempty"`
}

func (x *GetAllAvailableTimesRequest) Reset() {
	*x = GetAllAvailableTimesRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllAvailableTimesRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllAvailableTimesRequest) ProtoMessage() {}

func (x *GetAllAvailableTimesRequest) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllAvailableTimesRequest.ProtoReflect.Descriptor instead.
func (*GetAllAvailableTimesRequest) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{0}
}

func (x *GetAllAvailableTimesRequest) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

func (x *GetAllAvailableTimesRequest) GetAreaId() int64 {
	if x != nil {
		return x.AreaId
	}
	return 0
}

type GetAllAvailableTimesReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Categories *Categories `protobuf:"bytes,1,opt,name=categories,proto3" json:"categories,omitempty"`
}

func (x *GetAllAvailableTimesReply) Reset() {
	*x = GetAllAvailableTimesReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllAvailableTimesReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllAvailableTimesReply) ProtoMessage() {}

func (x *GetAllAvailableTimesReply) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllAvailableTimesReply.ProtoReflect.Descriptor instead.
func (*GetAllAvailableTimesReply) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{1}
}

func (x *GetAllAvailableTimesReply) GetCategories() *Categories {
	if x != nil {
		return x.Categories
	}
	return nil
}

type Categories struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Moments       int64 `protobuf:"varint,1,opt,name=moments,proto3" json:"moments,omitempty"`
	DirectMessage int64 `protobuf:"varint,2,opt,name=direct_message,json=directMessage,proto3" json:"direct_message,omitempty"`
	Coupons       int64 `protobuf:"varint,3,opt,name=coupons,proto3" json:"coupons,omitempty"`
}

func (x *Categories) Reset() {
	*x = Categories{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Categories) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Categories) ProtoMessage() {}

func (x *Categories) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Categories.ProtoReflect.Descriptor instead.
func (*Categories) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{2}
}

func (x *Categories) GetMoments() int64 {
	if x != nil {
		return x.Moments
	}
	return 0
}

func (x *Categories) GetDirectMessage() int64 {
	if x != nil {
		return x.DirectMessage
	}
	return 0
}

func (x *Categories) GetCoupons() int64 {
	if x != nil {
		return x.Coupons
	}
	return 0
}

type GetAvailableTimesRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Uid      string                            `protobuf:"bytes,1,opt,name=uid,proto3" json:"uid,omitempty"`
	Category GetAvailableTimesRequest_Category `protobuf:"varint,2,opt,name=category,proto3,enum=limitationservicepb.GetAvailableTimesRequest_Category" json:"category,omitempty"`
	AreaId   int64                             `protobuf:"varint,3,opt,name=area_id,json=areaId,proto3" json:"area_id,omitempty"`
}

func (x *GetAvailableTimesRequest) Reset() {
	*x = GetAvailableTimesRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAvailableTimesRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAvailableTimesRequest) ProtoMessage() {}

func (x *GetAvailableTimesRequest) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAvailableTimesRequest.ProtoReflect.Descriptor instead.
func (*GetAvailableTimesRequest) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{3}
}

func (x *GetAvailableTimesRequest) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

func (x *GetAvailableTimesRequest) GetCategory() GetAvailableTimesRequest_Category {
	if x != nil {
		return x.Category
	}
	return GetAvailableTimesRequest_MOMENTS
}

func (x *GetAvailableTimesRequest) GetAreaId() int64 {
	if x != nil {
		return x.AreaId
	}
	return 0
}

type GetAvailableTimesReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	CurrTimes int64 `protobuf:"varint,1,opt,name=curr_times,json=currTimes,proto3" json:"curr_times,omitempty"`
}

func (x *GetAvailableTimesReply) Reset() {
	*x = GetAvailableTimesReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAvailableTimesReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAvailableTimesReply) ProtoMessage() {}

func (x *GetAvailableTimesReply) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAvailableTimesReply.ProtoReflect.Descriptor instead.
func (*GetAvailableTimesReply) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{4}
}

func (x *GetAvailableTimesReply) GetCurrTimes() int64 {
	if x != nil {
		return x.CurrTimes
	}
	return 0
}

type StartDeductionRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Uid         string                         `protobuf:"bytes,1,opt,name=uid,proto3" json:"uid,omitempty"`
	Category    StartDeductionRequest_Category `protobuf:"varint,2,opt,name=category,proto3,enum=limitationservicepb.StartDeductionRequest_Category" json:"category,omitempty"`
	AreaId      int64                          `protobuf:"varint,3,opt,name=area_id,json=areaId,proto3" json:"area_id,omitempty"`
	DeductTimes *int64                         `protobuf:"varint,4,opt,name=deduct_times,json=deductTimes,proto3,oneof" json:"deduct_times,omitempty"`
}

func (x *StartDeductionRequest) Reset() {
	*x = StartDeductionRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StartDeductionRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StartDeductionRequest) ProtoMessage() {}

func (x *StartDeductionRequest) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StartDeductionRequest.ProtoReflect.Descriptor instead.
func (*StartDeductionRequest) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{5}
}

func (x *StartDeductionRequest) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

func (x *StartDeductionRequest) GetCategory() StartDeductionRequest_Category {
	if x != nil {
		return x.Category
	}
	return StartDeductionRequest_MOMENTS
}

func (x *StartDeductionRequest) GetAreaId() int64 {
	if x != nil {
		return x.AreaId
	}
	return 0
}

func (x *StartDeductionRequest) GetDeductTimes() int64 {
	if x != nil && x.DeductTimes != nil {
		return *x.DeductTimes
	}
	return 0
}

type StartDeductionReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TransactionId        string `protobuf:"bytes,1,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
	BeforeDeductionTimes int64  `protobuf:"varint,2,opt,name=before_deduction_times,json=beforeDeductionTimes,proto3" json:"before_deduction_times,omitempty"` // 事务开启前的余额
}

func (x *StartDeductionReply) Reset() {
	*x = StartDeductionReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StartDeductionReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StartDeductionReply) ProtoMessage() {}

func (x *StartDeductionReply) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StartDeductionReply.ProtoReflect.Descriptor instead.
func (*StartDeductionReply) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{6}
}

func (x *StartDeductionReply) GetTransactionId() string {
	if x != nil {
		return x.TransactionId
	}
	return ""
}

func (x *StartDeductionReply) GetBeforeDeductionTimes() int64 {
	if x != nil {
		return x.BeforeDeductionTimes
	}
	return 0
}

type CommitDeductionRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TransactionId string `protobuf:"bytes,1,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
}

func (x *CommitDeductionRequest) Reset() {
	*x = CommitDeductionRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CommitDeductionRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CommitDeductionRequest) ProtoMessage() {}

func (x *CommitDeductionRequest) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CommitDeductionRequest.ProtoReflect.Descriptor instead.
func (*CommitDeductionRequest) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{7}
}

func (x *CommitDeductionRequest) GetTransactionId() string {
	if x != nil {
		return x.TransactionId
	}
	return ""
}

type CommitDeductionReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AfterCommitTimes int64 `protobuf:"varint,1,opt,name=after_commit_times,json=afterCommitTimes,proto3" json:"after_commit_times,omitempty"` // 事务提交后的余额，应该为StartDeductionReply中的before_deduction_times的数值减掉1或deduct_times指定的数额
}

func (x *CommitDeductionReply) Reset() {
	*x = CommitDeductionReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CommitDeductionReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CommitDeductionReply) ProtoMessage() {}

func (x *CommitDeductionReply) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CommitDeductionReply.ProtoReflect.Descriptor instead.
func (*CommitDeductionReply) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{8}
}

func (x *CommitDeductionReply) GetAfterCommitTimes() int64 {
	if x != nil {
		return x.AfterCommitTimes
	}
	return 0
}

type RollbackDeductionRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TransactionId string `protobuf:"bytes,1,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
}

func (x *RollbackDeductionRequest) Reset() {
	*x = RollbackDeductionRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RollbackDeductionRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RollbackDeductionRequest) ProtoMessage() {}

func (x *RollbackDeductionRequest) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RollbackDeductionRequest.ProtoReflect.Descriptor instead.
func (*RollbackDeductionRequest) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{9}
}

func (x *RollbackDeductionRequest) GetTransactionId() string {
	if x != nil {
		return x.TransactionId
	}
	return ""
}

type RollbackDeductionReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AfterRollbackTimes int64 `protobuf:"varint,1,opt,name=after_rollback_times,json=afterRollbackTimes,proto3" json:"after_rollback_times,omitempty"` // 事务回滚后的余额，应该与StartDeductionReply中的before_deduction_times的数值一致
}

func (x *RollbackDeductionReply) Reset() {
	*x = RollbackDeductionReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_limitationservice_proto_msgTypes[10]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RollbackDeductionReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RollbackDeductionReply) ProtoMessage() {}

func (x *RollbackDeductionReply) ProtoReflect() protoreflect.Message {
	mi := &file_limitationservice_proto_msgTypes[10]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RollbackDeductionReply.ProtoReflect.Descriptor instead.
func (*RollbackDeductionReply) Descriptor() ([]byte, []int) {
	return file_limitationservice_proto_rawDescGZIP(), []int{10}
}

func (x *RollbackDeductionReply) GetAfterRollbackTimes() int64 {
	if x != nil {
		return x.AfterRollbackTimes
	}
	return 0
}

var File_limitationservice_proto protoreflect.FileDescriptor

var file_limitationservice_proto_rawDesc = []byte{
	0x0a, 0x17, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x13, 0x6c, 0x69, 0x6d, 0x69, 0x74,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x22, 0x48,
	0x0a, 0x1b, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61, 0x62, 0x6c,
	0x65, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a,
	0x03, 0x75, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69, 0x64, 0x12,
	0x17, 0x0a, 0x07, 0x61, 0x72, 0x65, 0x61, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x06, 0x61, 0x72, 0x65, 0x61, 0x49, 0x64, 0x22, 0x5c, 0x0a, 0x19, 0x47, 0x65, 0x74, 0x41,
	0x6c, 0x6c, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61, 0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x73,
	0x52, 0x65, 0x70, 0x6c, 0x79, 0x12, 0x3f, 0x0a, 0x0a, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72,
	0x69, 0x65, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1f, 0x2e, 0x6c, 0x69, 0x6d, 0x69,
	0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e,
	0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x69, 0x65, 0x73, 0x52, 0x0a, 0x63, 0x61, 0x74, 0x65,
	0x67, 0x6f, 0x72, 0x69, 0x65, 0x73, 0x22, 0x67, 0x0a, 0x0a, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f,
	0x72, 0x69, 0x65, 0x73, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x6f, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x6d, 0x6f, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x12, 0x25,
	0x0a, 0x0e, 0x64, 0x69, 0x72, 0x65, 0x63, 0x74, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0d, 0x64, 0x69, 0x72, 0x65, 0x63, 0x74, 0x4d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x63, 0x6f, 0x75, 0x70, 0x6f, 0x6e, 0x73,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x63, 0x6f, 0x75, 0x70, 0x6f, 0x6e, 0x73, 0x22,
	0xd3, 0x01, 0x0a, 0x18, 0x47, 0x65, 0x74, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61, 0x62, 0x6c, 0x65,
	0x54, 0x69, 0x6d, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a, 0x03,
	0x75, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69, 0x64, 0x12, 0x52,
	0x0a, 0x08, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e,
	0x32, 0x36, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61,
	0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e,
	0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x52, 0x08, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f,
	0x72, 0x79, 0x12, 0x17, 0x0a, 0x07, 0x61, 0x72, 0x65, 0x61, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x06, 0x61, 0x72, 0x65, 0x61, 0x49, 0x64, 0x22, 0x38, 0x0a, 0x08, 0x43,
	0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x0b, 0x0a, 0x07, 0x4d, 0x4f, 0x4d, 0x45, 0x4e,
	0x54, 0x53, 0x10, 0x00, 0x12, 0x12, 0x0a, 0x0e, 0x44, 0x49, 0x52, 0x45, 0x43, 0x54, 0x5f, 0x4d,
	0x45, 0x53, 0x53, 0x41, 0x47, 0x45, 0x10, 0x01, 0x12, 0x0b, 0x0a, 0x07, 0x43, 0x4f, 0x55, 0x50,
	0x4f, 0x4e, 0x53, 0x10, 0x02, 0x22, 0x37, 0x0a, 0x16, 0x47, 0x65, 0x74, 0x41, 0x76, 0x61, 0x69,
	0x6c, 0x61, 0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x12,
	0x1d, 0x0a, 0x0a, 0x63, 0x75, 0x72, 0x72, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x09, 0x63, 0x75, 0x72, 0x72, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x22, 0x86,
	0x02, 0x0a, 0x15, 0x53, 0x74, 0x61, 0x72, 0x74, 0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a, 0x03, 0x75, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69, 0x64, 0x12, 0x4f, 0x0a, 0x08, 0x63, 0x61,
	0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x33, 0x2e, 0x6c,
	0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x70, 0x62, 0x2e, 0x53, 0x74, 0x61, 0x72, 0x74, 0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72,
	0x79, 0x52, 0x08, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x17, 0x0a, 0x07, 0x61,
	0x72, 0x65, 0x61, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x61, 0x72,
	0x65, 0x61, 0x49, 0x64, 0x12, 0x26, 0x0a, 0x0c, 0x64, 0x65, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x74,
	0x69, 0x6d, 0x65, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x48, 0x00, 0x52, 0x0b, 0x64, 0x65,
	0x64, 0x75, 0x63, 0x74, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x88, 0x01, 0x01, 0x22, 0x38, 0x0a, 0x08,
	0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x0b, 0x0a, 0x07, 0x4d, 0x4f, 0x4d, 0x45,
	0x4e, 0x54, 0x53, 0x10, 0x00, 0x12, 0x12, 0x0a, 0x0e, 0x44, 0x49, 0x52, 0x45, 0x43, 0x54, 0x5f,
	0x4d, 0x45, 0x53, 0x53, 0x41, 0x47, 0x45, 0x10, 0x01, 0x12, 0x0b, 0x0a, 0x07, 0x43, 0x4f, 0x55,
	0x50, 0x4f, 0x4e, 0x53, 0x10, 0x02, 0x42, 0x0f, 0x0a, 0x0d, 0x5f, 0x64, 0x65, 0x64, 0x75, 0x63,
	0x74, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x22, 0x72, 0x0a, 0x13, 0x53, 0x74, 0x61, 0x72, 0x74,
	0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x12, 0x25,
	0x0a, 0x0e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x69, 0x64,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74,
	0x69, 0x6f, 0x6e, 0x49, 0x64, 0x12, 0x34, 0x0a, 0x16, 0x62, 0x65, 0x66, 0x6f, 0x72, 0x65, 0x5f,
	0x64, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x14, 0x62, 0x65, 0x66, 0x6f, 0x72, 0x65, 0x44, 0x65, 0x64,
	0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x22, 0x3f, 0x0a, 0x16, 0x43,
	0x6f, 0x6d, 0x6d, 0x69, 0x74, 0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x25, 0x0a, 0x0e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x61, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x74,
	0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x49, 0x64, 0x22, 0x44, 0x0a, 0x14,
	0x43, 0x6f, 0x6d, 0x6d, 0x69, 0x74, 0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52,
	0x65, 0x70, 0x6c, 0x79, 0x12, 0x2c, 0x0a, 0x12, 0x61, 0x66, 0x74, 0x65, 0x72, 0x5f, 0x63, 0x6f,
	0x6d, 0x6d, 0x69, 0x74, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x10, 0x61, 0x66, 0x74, 0x65, 0x72, 0x43, 0x6f, 0x6d, 0x6d, 0x69, 0x74, 0x54, 0x69, 0x6d,
	0x65, 0x73, 0x22, 0x41, 0x0a, 0x18, 0x52, 0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63, 0x6b, 0x44, 0x65,
	0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x25,
	0x0a, 0x0e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x69, 0x64,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74,
	0x69, 0x6f, 0x6e, 0x49, 0x64, 0x22, 0x4a, 0x0a, 0x16, 0x52, 0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63,
	0x6b, 0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x12,
	0x30, 0x0a, 0x14, 0x61, 0x66, 0x74, 0x65, 0x72, 0x5f, 0x72, 0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63,
	0x6b, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x12, 0x61,
	0x66, 0x74, 0x65, 0x72, 0x52, 0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63, 0x6b, 0x54, 0x69, 0x6d, 0x65,
	0x73, 0x32, 0xcc, 0x04, 0x0a, 0x11, 0x4c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x7a, 0x0a, 0x14, 0x47, 0x65, 0x74, 0x41, 0x6c,
	0x6c, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61, 0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x12,
	0x30, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x41, 0x76, 0x61, 0x69,
	0x6c, 0x61, 0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x2e, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x41, 0x76,
	0x61, 0x69, 0x6c, 0x61, 0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x52, 0x65, 0x70, 0x6c,
	0x79, 0x22, 0x00, 0x12, 0x71, 0x0a, 0x11, 0x47, 0x65, 0x74, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61,
	0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x12, 0x2d, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x47,
	0x65, 0x74, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61, 0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x73,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x2b, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x47, 0x65,
	0x74, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61, 0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x52,
	0x65, 0x70, 0x6c, 0x79, 0x22, 0x00, 0x12, 0x68, 0x0a, 0x0e, 0x53, 0x74, 0x61, 0x72, 0x74, 0x44,
	0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x2a, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x53,
	0x74, 0x61, 0x72, 0x74, 0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x28, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x53, 0x74, 0x61, 0x72, 0x74,
	0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x22, 0x00,
	0x12, 0x6b, 0x0a, 0x0f, 0x43, 0x6f, 0x6d, 0x6d, 0x69, 0x74, 0x44, 0x65, 0x64, 0x75, 0x63, 0x74,
	0x69, 0x6f, 0x6e, 0x12, 0x2b, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x43, 0x6f, 0x6d, 0x6d, 0x69, 0x74,
	0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x29, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x43, 0x6f, 0x6d, 0x6d, 0x69, 0x74, 0x44, 0x65, 0x64,
	0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x22, 0x00, 0x12, 0x71, 0x0a,
	0x11, 0x52, 0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63, 0x6b, 0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69,
	0x6f, 0x6e, 0x12, 0x2d, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x52, 0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63,
	0x6b, 0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x2b, 0x2e, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x70, 0x62, 0x2e, 0x52, 0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63, 0x6b,
	0x44, 0x65, 0x64, 0x75, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x22, 0x00,
	0x42, 0x50, 0x5a, 0x4e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x68,
	0x65, 0x6e, 0x72, 0x79, 0x30, 0x34, 0x37, 0x35, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x73, 0x2f, 0x79, 0x75, 0x6d, 0x6f, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x2f, 0x6c, 0x69, 0x6d,
	0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x6c,
	0x69, 0x6d, 0x69, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_limitationservice_proto_rawDescOnce sync.Once
	file_limitationservice_proto_rawDescData = file_limitationservice_proto_rawDesc
)

func file_limitationservice_proto_rawDescGZIP() []byte {
	file_limitationservice_proto_rawDescOnce.Do(func() {
		file_limitationservice_proto_rawDescData = protoimpl.X.CompressGZIP(file_limitationservice_proto_rawDescData)
	})
	return file_limitationservice_proto_rawDescData
}

var file_limitationservice_proto_enumTypes = make([]protoimpl.EnumInfo, 2)
var file_limitationservice_proto_msgTypes = make([]protoimpl.MessageInfo, 11)
var file_limitationservice_proto_goTypes = []interface{}{
	(GetAvailableTimesRequest_Category)(0), // 0: limitationservicepb.GetAvailableTimesRequest.Category
	(StartDeductionRequest_Category)(0),    // 1: limitationservicepb.StartDeductionRequest.Category
	(*GetAllAvailableTimesRequest)(nil),    // 2: limitationservicepb.GetAllAvailableTimesRequest
	(*GetAllAvailableTimesReply)(nil),      // 3: limitationservicepb.GetAllAvailableTimesReply
	(*Categories)(nil),                     // 4: limitationservicepb.Categories
	(*GetAvailableTimesRequest)(nil),       // 5: limitationservicepb.GetAvailableTimesRequest
	(*GetAvailableTimesReply)(nil),         // 6: limitationservicepb.GetAvailableTimesReply
	(*StartDeductionRequest)(nil),          // 7: limitationservicepb.StartDeductionRequest
	(*StartDeductionReply)(nil),            // 8: limitationservicepb.StartDeductionReply
	(*CommitDeductionRequest)(nil),         // 9: limitationservicepb.CommitDeductionRequest
	(*CommitDeductionReply)(nil),           // 10: limitationservicepb.CommitDeductionReply
	(*RollbackDeductionRequest)(nil),       // 11: limitationservicepb.RollbackDeductionRequest
	(*RollbackDeductionReply)(nil),         // 12: limitationservicepb.RollbackDeductionReply
}
var file_limitationservice_proto_depIdxs = []int32{
	4,  // 0: limitationservicepb.GetAllAvailableTimesReply.categories:type_name -> limitationservicepb.Categories
	0,  // 1: limitationservicepb.GetAvailableTimesRequest.category:type_name -> limitationservicepb.GetAvailableTimesRequest.Category
	1,  // 2: limitationservicepb.StartDeductionRequest.category:type_name -> limitationservicepb.StartDeductionRequest.Category
	2,  // 3: limitationservicepb.LimitationService.GetAllAvailableTimes:input_type -> limitationservicepb.GetAllAvailableTimesRequest
	5,  // 4: limitationservicepb.LimitationService.GetAvailableTimes:input_type -> limitationservicepb.GetAvailableTimesRequest
	7,  // 5: limitationservicepb.LimitationService.StartDeduction:input_type -> limitationservicepb.StartDeductionRequest
	9,  // 6: limitationservicepb.LimitationService.CommitDeduction:input_type -> limitationservicepb.CommitDeductionRequest
	11, // 7: limitationservicepb.LimitationService.RollbackDeduction:input_type -> limitationservicepb.RollbackDeductionRequest
	3,  // 8: limitationservicepb.LimitationService.GetAllAvailableTimes:output_type -> limitationservicepb.GetAllAvailableTimesReply
	6,  // 9: limitationservicepb.LimitationService.GetAvailableTimes:output_type -> limitationservicepb.GetAvailableTimesReply
	8,  // 10: limitationservicepb.LimitationService.StartDeduction:output_type -> limitationservicepb.StartDeductionReply
	10, // 11: limitationservicepb.LimitationService.CommitDeduction:output_type -> limitationservicepb.CommitDeductionReply
	12, // 12: limitationservicepb.LimitationService.RollbackDeduction:output_type -> limitationservicepb.RollbackDeductionReply
	8,  // [8:13] is the sub-list for method output_type
	3,  // [3:8] is the sub-list for method input_type
	3,  // [3:3] is the sub-list for extension type_name
	3,  // [3:3] is the sub-list for extension extendee
	0,  // [0:3] is the sub-list for field type_name
}

func init() { file_limitationservice_proto_init() }
func file_limitationservice_proto_init() {
	if File_limitationservice_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_limitationservice_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAllAvailableTimesRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAllAvailableTimesReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Categories); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAvailableTimesRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAvailableTimesReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StartDeductionRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StartDeductionReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CommitDeductionRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CommitDeductionReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[9].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RollbackDeductionRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_limitationservice_proto_msgTypes[10].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RollbackDeductionReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_limitationservice_proto_msgTypes[5].OneofWrappers = []interface{}{}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_limitationservice_proto_rawDesc,
			NumEnums:      2,
			NumMessages:   11,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_limitationservice_proto_goTypes,
		DependencyIndexes: file_limitationservice_proto_depIdxs,
		EnumInfos:         file_limitationservice_proto_enumTypes,
		MessageInfos:      file_limitationservice_proto_msgTypes,
	}.Build()
	File_limitationservice_proto = out.File
	file_limitationservice_proto_rawDesc = nil
	file_limitationservice_proto_goTypes = nil
	file_limitationservice_proto_depIdxs = nil
}
