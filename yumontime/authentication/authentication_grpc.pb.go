// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.2
// source: authentication.proto

package authenticationpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// AuthenticationServiceClient is the client API for AuthenticationService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type AuthenticationServiceClient interface {
	// SendMobileChallengeCode for sending sms code to users
	SendMobileChallengeCode(ctx context.Context, in *SendMobileChallengeCodeRequest, opts ...grpc.CallOption) (*SendMobileChallengeCodeReply, error)
	// VerifyMobileChallengeCode for verifying challenge code
	VerifyMobileChallengeCode(ctx context.Context, in *VerifyMobileChallengeCodeRequest, opts ...grpc.CallOption) (*VerifyMobileChallengeCodeReply, error)
	SendEmailChallengeCode(ctx context.Context, in *SendEmailChallengeCodeRequest, opts ...grpc.CallOption) (*SendEmailChallengeCodeReply, error)
	VerifyEmailChallengeCode(ctx context.Context, in *VerifyEmailChallengeCodeRequest, opts ...grpc.CallOption) (*VerifyEmailChallengeCodeReply, error)
}

type authenticationServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewAuthenticationServiceClient(cc grpc.ClientConnInterface) AuthenticationServiceClient {
	return &authenticationServiceClient{cc}
}

func (c *authenticationServiceClient) SendMobileChallengeCode(ctx context.Context, in *SendMobileChallengeCodeRequest, opts ...grpc.CallOption) (*SendMobileChallengeCodeReply, error) {
	out := new(SendMobileChallengeCodeReply)
	err := c.cc.Invoke(ctx, "/authenticationservicepb.AuthenticationService/SendMobileChallengeCode", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *authenticationServiceClient) VerifyMobileChallengeCode(ctx context.Context, in *VerifyMobileChallengeCodeRequest, opts ...grpc.CallOption) (*VerifyMobileChallengeCodeReply, error) {
	out := new(VerifyMobileChallengeCodeReply)
	err := c.cc.Invoke(ctx, "/authenticationservicepb.AuthenticationService/VerifyMobileChallengeCode", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *authenticationServiceClient) SendEmailChallengeCode(ctx context.Context, in *SendEmailChallengeCodeRequest, opts ...grpc.CallOption) (*SendEmailChallengeCodeReply, error) {
	out := new(SendEmailChallengeCodeReply)
	err := c.cc.Invoke(ctx, "/authenticationservicepb.AuthenticationService/SendEmailChallengeCode", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *authenticationServiceClient) VerifyEmailChallengeCode(ctx context.Context, in *VerifyEmailChallengeCodeRequest, opts ...grpc.CallOption) (*VerifyEmailChallengeCodeReply, error) {
	out := new(VerifyEmailChallengeCodeReply)
	err := c.cc.Invoke(ctx, "/authenticationservicepb.AuthenticationService/VerifyEmailChallengeCode", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AuthenticationServiceServer is the server API for AuthenticationService service.
// All implementations must embed UnimplementedAuthenticationServiceServer
// for forward compatibility
type AuthenticationServiceServer interface {
	// SendMobileChallengeCode for sending sms code to users
	SendMobileChallengeCode(context.Context, *SendMobileChallengeCodeRequest) (*SendMobileChallengeCodeReply, error)
	// VerifyMobileChallengeCode for verifying challenge code
	VerifyMobileChallengeCode(context.Context, *VerifyMobileChallengeCodeRequest) (*VerifyMobileChallengeCodeReply, error)
	SendEmailChallengeCode(context.Context, *SendEmailChallengeCodeRequest) (*SendEmailChallengeCodeReply, error)
	VerifyEmailChallengeCode(context.Context, *VerifyEmailChallengeCodeRequest) (*VerifyEmailChallengeCodeReply, error)
	mustEmbedUnimplementedAuthenticationServiceServer()
}

// UnimplementedAuthenticationServiceServer must be embedded to have forward compatible implementations.
type UnimplementedAuthenticationServiceServer struct {
}

func (UnimplementedAuthenticationServiceServer) SendMobileChallengeCode(context.Context, *SendMobileChallengeCodeRequest) (*SendMobileChallengeCodeReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendMobileChallengeCode not implemented")
}
func (UnimplementedAuthenticationServiceServer) VerifyMobileChallengeCode(context.Context, *VerifyMobileChallengeCodeRequest) (*VerifyMobileChallengeCodeReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method VerifyMobileChallengeCode not implemented")
}
func (UnimplementedAuthenticationServiceServer) SendEmailChallengeCode(context.Context, *SendEmailChallengeCodeRequest) (*SendEmailChallengeCodeReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendEmailChallengeCode not implemented")
}
func (UnimplementedAuthenticationServiceServer) VerifyEmailChallengeCode(context.Context, *VerifyEmailChallengeCodeRequest) (*VerifyEmailChallengeCodeReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method VerifyEmailChallengeCode not implemented")
}
func (UnimplementedAuthenticationServiceServer) mustEmbedUnimplementedAuthenticationServiceServer() {}

// UnsafeAuthenticationServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to AuthenticationServiceServer will
// result in compilation errors.
type UnsafeAuthenticationServiceServer interface {
	mustEmbedUnimplementedAuthenticationServiceServer()
}

func RegisterAuthenticationServiceServer(s grpc.ServiceRegistrar, srv AuthenticationServiceServer) {
	s.RegisterService(&AuthenticationService_ServiceDesc, srv)
}

func _AuthenticationService_SendMobileChallengeCode_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendMobileChallengeCodeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthenticationServiceServer).SendMobileChallengeCode(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/authenticationservicepb.AuthenticationService/SendMobileChallengeCode",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthenticationServiceServer).SendMobileChallengeCode(ctx, req.(*SendMobileChallengeCodeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AuthenticationService_VerifyMobileChallengeCode_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(VerifyMobileChallengeCodeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthenticationServiceServer).VerifyMobileChallengeCode(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/authenticationservicepb.AuthenticationService/VerifyMobileChallengeCode",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthenticationServiceServer).VerifyMobileChallengeCode(ctx, req.(*VerifyMobileChallengeCodeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AuthenticationService_SendEmailChallengeCode_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendEmailChallengeCodeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthenticationServiceServer).SendEmailChallengeCode(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/authenticationservicepb.AuthenticationService/SendEmailChallengeCode",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthenticationServiceServer).SendEmailChallengeCode(ctx, req.(*SendEmailChallengeCodeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AuthenticationService_VerifyEmailChallengeCode_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(VerifyEmailChallengeCodeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthenticationServiceServer).VerifyEmailChallengeCode(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/authenticationservicepb.AuthenticationService/VerifyEmailChallengeCode",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthenticationServiceServer).VerifyEmailChallengeCode(ctx, req.(*VerifyEmailChallengeCodeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// AuthenticationService_ServiceDesc is the grpc.ServiceDesc for AuthenticationService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var AuthenticationService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "authenticationservicepb.AuthenticationService",
	HandlerType: (*AuthenticationServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SendMobileChallengeCode",
			Handler:    _AuthenticationService_SendMobileChallengeCode_Handler,
		},
		{
			MethodName: "VerifyMobileChallengeCode",
			Handler:    _AuthenticationService_VerifyMobileChallengeCode_Handler,
		},
		{
			MethodName: "SendEmailChallengeCode",
			Handler:    _AuthenticationService_SendEmailChallengeCode_Handler,
		},
		{
			MethodName: "VerifyEmailChallengeCode",
			Handler:    _AuthenticationService_VerifyEmailChallengeCode_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "authentication.proto",
}
