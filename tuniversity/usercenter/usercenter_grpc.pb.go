// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package usercenterpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// UserCenterServiceClient is the client API for UserCenterService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type UserCenterServiceClient interface {
	GetUser(ctx context.Context, in *GetUserRequest, opts ...grpc.CallOption) (*GetUserReply, error)
	GetUsers(ctx context.Context, in *GetUsersRequest, opts ...grpc.CallOption) (*GetUsersReply, error)
	GetMemberCard(ctx context.Context, in *GetMemberCardRequest, opts ...grpc.CallOption) (*GetMemberCardReply, error)
	RegisterUser(ctx context.Context, in *RegisterUserRequest, opts ...grpc.CallOption) (*RegisterUserReply, error)
	GetUID(ctx context.Context, in *GetUIDRequest, opts ...grpc.CallOption) (*GetUIDReply, error)
	GetBadges(ctx context.Context, in *GetBadgesRequest, opts ...grpc.CallOption) (*GetBadgesReply, error)
	IssueBadge(ctx context.Context, in *IssueBadgeRequest, opts ...grpc.CallOption) (*IssueBadgeReply, error)
	RevokeBadge(ctx context.Context, in *RevokeBadgeRequest, opts ...grpc.CallOption) (*RevokeBadgeReply, error)
}

type userCenterServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewUserCenterServiceClient(cc grpc.ClientConnInterface) UserCenterServiceClient {
	return &userCenterServiceClient{cc}
}

func (c *userCenterServiceClient) GetUser(ctx context.Context, in *GetUserRequest, opts ...grpc.CallOption) (*GetUserReply, error) {
	out := new(GetUserReply)
	err := c.cc.Invoke(ctx, "/UserCenterService/GetUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterServiceClient) GetUsers(ctx context.Context, in *GetUsersRequest, opts ...grpc.CallOption) (*GetUsersReply, error) {
	out := new(GetUsersReply)
	err := c.cc.Invoke(ctx, "/UserCenterService/GetUsers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterServiceClient) GetMemberCard(ctx context.Context, in *GetMemberCardRequest, opts ...grpc.CallOption) (*GetMemberCardReply, error) {
	out := new(GetMemberCardReply)
	err := c.cc.Invoke(ctx, "/UserCenterService/GetMemberCard", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterServiceClient) RegisterUser(ctx context.Context, in *RegisterUserRequest, opts ...grpc.CallOption) (*RegisterUserReply, error) {
	out := new(RegisterUserReply)
	err := c.cc.Invoke(ctx, "/UserCenterService/RegisterUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterServiceClient) GetUID(ctx context.Context, in *GetUIDRequest, opts ...grpc.CallOption) (*GetUIDReply, error) {
	out := new(GetUIDReply)
	err := c.cc.Invoke(ctx, "/UserCenterService/GetUID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterServiceClient) GetBadges(ctx context.Context, in *GetBadgesRequest, opts ...grpc.CallOption) (*GetBadgesReply, error) {
	out := new(GetBadgesReply)
	err := c.cc.Invoke(ctx, "/UserCenterService/GetBadges", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterServiceClient) IssueBadge(ctx context.Context, in *IssueBadgeRequest, opts ...grpc.CallOption) (*IssueBadgeReply, error) {
	out := new(IssueBadgeReply)
	err := c.cc.Invoke(ctx, "/UserCenterService/IssueBadge", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterServiceClient) RevokeBadge(ctx context.Context, in *RevokeBadgeRequest, opts ...grpc.CallOption) (*RevokeBadgeReply, error) {
	out := new(RevokeBadgeReply)
	err := c.cc.Invoke(ctx, "/UserCenterService/RevokeBadge", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UserCenterServiceServer is the server API for UserCenterService service.
// All implementations must embed UnimplementedUserCenterServiceServer
// for forward compatibility
type UserCenterServiceServer interface {
	GetUser(context.Context, *GetUserRequest) (*GetUserReply, error)
	GetUsers(context.Context, *GetUsersRequest) (*GetUsersReply, error)
	GetMemberCard(context.Context, *GetMemberCardRequest) (*GetMemberCardReply, error)
	RegisterUser(context.Context, *RegisterUserRequest) (*RegisterUserReply, error)
	GetUID(context.Context, *GetUIDRequest) (*GetUIDReply, error)
	GetBadges(context.Context, *GetBadgesRequest) (*GetBadgesReply, error)
	IssueBadge(context.Context, *IssueBadgeRequest) (*IssueBadgeReply, error)
	RevokeBadge(context.Context, *RevokeBadgeRequest) (*RevokeBadgeReply, error)
	mustEmbedUnimplementedUserCenterServiceServer()
}

// UnimplementedUserCenterServiceServer must be embedded to have forward compatible implementations.
type UnimplementedUserCenterServiceServer struct {
}

func (UnimplementedUserCenterServiceServer) GetUser(context.Context, *GetUserRequest) (*GetUserReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetUser not implemented")
}
func (UnimplementedUserCenterServiceServer) GetUsers(context.Context, *GetUsersRequest) (*GetUsersReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetUsers not implemented")
}
func (UnimplementedUserCenterServiceServer) GetMemberCard(context.Context, *GetMemberCardRequest) (*GetMemberCardReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMemberCard not implemented")
}
func (UnimplementedUserCenterServiceServer) RegisterUser(context.Context, *RegisterUserRequest) (*RegisterUserReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RegisterUser not implemented")
}
func (UnimplementedUserCenterServiceServer) GetUID(context.Context, *GetUIDRequest) (*GetUIDReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetUID not implemented")
}
func (UnimplementedUserCenterServiceServer) GetBadges(context.Context, *GetBadgesRequest) (*GetBadgesReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetBadges not implemented")
}
func (UnimplementedUserCenterServiceServer) IssueBadge(context.Context, *IssueBadgeRequest) (*IssueBadgeReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method IssueBadge not implemented")
}
func (UnimplementedUserCenterServiceServer) RevokeBadge(context.Context, *RevokeBadgeRequest) (*RevokeBadgeReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RevokeBadge not implemented")
}
func (UnimplementedUserCenterServiceServer) mustEmbedUnimplementedUserCenterServiceServer() {}

// UnsafeUserCenterServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to UserCenterServiceServer will
// result in compilation errors.
type UnsafeUserCenterServiceServer interface {
	mustEmbedUnimplementedUserCenterServiceServer()
}

func RegisterUserCenterServiceServer(s grpc.ServiceRegistrar, srv UserCenterServiceServer) {
	s.RegisterService(&UserCenterService_ServiceDesc, srv)
}

func _UserCenterService_GetUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServiceServer).GetUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/UserCenterService/GetUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServiceServer).GetUser(ctx, req.(*GetUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenterService_GetUsers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetUsersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServiceServer).GetUsers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/UserCenterService/GetUsers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServiceServer).GetUsers(ctx, req.(*GetUsersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenterService_GetMemberCard_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetMemberCardRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServiceServer).GetMemberCard(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/UserCenterService/GetMemberCard",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServiceServer).GetMemberCard(ctx, req.(*GetMemberCardRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenterService_RegisterUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RegisterUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServiceServer).RegisterUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/UserCenterService/RegisterUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServiceServer).RegisterUser(ctx, req.(*RegisterUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenterService_GetUID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetUIDRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServiceServer).GetUID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/UserCenterService/GetUID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServiceServer).GetUID(ctx, req.(*GetUIDRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenterService_GetBadges_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetBadgesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServiceServer).GetBadges(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/UserCenterService/GetBadges",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServiceServer).GetBadges(ctx, req.(*GetBadgesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenterService_IssueBadge_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IssueBadgeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServiceServer).IssueBadge(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/UserCenterService/IssueBadge",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServiceServer).IssueBadge(ctx, req.(*IssueBadgeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenterService_RevokeBadge_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RevokeBadgeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServiceServer).RevokeBadge(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/UserCenterService/RevokeBadge",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServiceServer).RevokeBadge(ctx, req.(*RevokeBadgeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// UserCenterService_ServiceDesc is the grpc.ServiceDesc for UserCenterService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var UserCenterService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "UserCenterService",
	HandlerType: (*UserCenterServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetUser",
			Handler:    _UserCenterService_GetUser_Handler,
		},
		{
			MethodName: "GetUsers",
			Handler:    _UserCenterService_GetUsers_Handler,
		},
		{
			MethodName: "GetMemberCard",
			Handler:    _UserCenterService_GetMemberCard_Handler,
		},
		{
			MethodName: "RegisterUser",
			Handler:    _UserCenterService_RegisterUser_Handler,
		},
		{
			MethodName: "GetUID",
			Handler:    _UserCenterService_GetUID_Handler,
		},
		{
			MethodName: "GetBadges",
			Handler:    _UserCenterService_GetBadges_Handler,
		},
		{
			MethodName: "IssueBadge",
			Handler:    _UserCenterService_IssueBadge_Handler,
		},
		{
			MethodName: "RevokeBadge",
			Handler:    _UserCenterService_RevokeBadge_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "tuniversity/usercenter/usercenter.proto",
}
